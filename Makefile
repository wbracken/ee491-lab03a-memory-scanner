###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 03a - memScan - EE 491F - Spr 2022
###
### @file    Makefile
### @version 1.0 - memory scanner lab
###
### Build and test memory scanner program
###
### @author  Will Bracken <wbracken@hawaii.edu>
### @date  08_Feb_2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = memScan

all: $(TARGET)

wc: memScan.c
	$(CC) $(CFLAGS)  -o $(TARGET) memScan.c

test: memScan
	./memScan /proc/self/maps
	
clean:
	rm -f $(TARGET)

