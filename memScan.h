/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab 03a - memScan - EE 491 - Spr 2022
///
/// @file    memScan.h
/// @version 1.0 - memScan lab assignment
///
/// Project will open and read proc/self/maps, scan the region and print memory
/// address, permissions, number of bytes read, and count the "A"s.
///
/// @author  Will Bracken <wbracken@hawaii.edu>
/// @date    08_Feb_2022
///
/// @see     https://linuxcommand.org/lc3_man_pages/wc1.html
///
///////////////////////////////////////////////////////////////////////////////
#define FILENAME "memScan"
#define MAX 4096


typedef struct {

   char startAddress[MAX];
   char endAddress[MAX];
   char permissions[20];
   int procBytes;
   int procAs;


} procData;



