/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab 03a - memScan - EE 491 - Spr 2022
///
/// @file    memScan.c
/// @version 1.0 - memScan lab assignment
///
/// Project will open and read proc/self/maps, scan the region and print memory
/// address, permissions, number of bytes read, and count the "A"s.
///
/// @author  Will Bracken <wbracken@hawaii.edu>
/// @date    08_Feb_2022
///
/// @see     https://linuxcommand.org/lc3_man_pages/wc1.html
///
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "memScan.h"


//@todo move to separate file 
int scanStruct(procData procMaps, int numProcs){
   int numBytes=0, numAs=0; 
   
  if (strstr (procMaps.permissions,"r") != NULL){
      char* currentAddress = procMaps.startAddress;
      char* stopAddress = procMaps.endAddress;
      sscanf(procMaps.startAddress, "%p", &currentAddress);
      sscanf(procMaps.endAddress, "%p", &stopAddress);
      while(currentAddress != stopAddress){
         if(*currentAddress == 'A') {
         numAs += 1;
         }
         numBytes+= 1;
         currentAddress = currentAddress + 1;
      }   
  }

   printf("%d: %s-%s   %s   Number of bytes read [%d]   Number of 'A' is[%d]\n", numProcs,
      procMaps.startAddress,procMaps.endAddress,procMaps.permissions, numBytes, numAs);


   return 0;
}


int main( int argc, char* argv[] ) {

   //wrong number of inputs
   if (argc != 2){
      fprintf(stderr, "usage: [%s] /proc/self/maps\n", FILENAME);
      exit(EXIT_FAILURE);
    //incorrect input   
    } else if( !( strcmp( argv[1], "/proc/self/maps" ) == 0 ) ){
    fprintf(stderr, "usage: [%s] /proc/self/maps\n", FILENAME);
    exit(EXIT_FAILURE);
    }
    
    FILE* fp = NULL;

    fp = fopen(argv[1], "r");

    char* lineIn;
    size_t len = 0;
    ssize_t read;
    int numProcs = 0;
    char* addressWhole;
    char* addressStart;
    char* addressEnd;
    char* permissions;
    
    procData procMapsData = {" ", " ", " "};

    while ( ( read = getline( &lineIn, &len, fp ) ) != -1 ){

      if(strstr(lineIn,"vvar")){
      exit(EXIT_SUCCESS);

      }

         addressWhole = strtok(lineIn, " ");

         permissions = strtok(NULL, " ");

         addressStart = strtok(addressWhole, "-");

         addressEnd = strtok(NULL, "-");

         strcpy(procMapsData.startAddress, addressStart);
         strcpy(procMapsData.endAddress, addressEnd);
         strcpy(procMapsData.permissions, permissions);
       
         scanStruct(procMapsData, numProcs);
         numProcs++;

    }


    exit(EXIT_SUCCESS);

   

}
